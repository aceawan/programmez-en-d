[set
    title       = "''Hello World'' !"
    partAs      = chapitre
    translator  = "Raphaël Jakse"
    proofreader = "Stéphane Goujet"
]

Le premier programme montré dans la plupart des livres qui présentent un langage de programmation est le programme ''hello world''. C'est un programme très simple et très court qui affiche "hello world" et qui se termine (NdT~ : "hello world" signifie "bonjour le monde"). Ce programme est important parce qu'il présente quelques concepts essentiels de ce langage.

Voilà ce que ça donne en langage D :

[code=d <<<
   import std.stdio;

   void main()
   {
       writeln("Hello world!");
   }
>>>]

Le ''code source'' ci-dessus doit être compilé par un compilateur D pour obtenir un programme exécutable par l'ordinateur, l'ordinateur ne pouvant pas directement exécuter le code source.

[ = Installation du compilateur.

   Lors de l'écriture de ce chapitre, nous avons le choix entre trois compilateurs~ :
      - [c dmd], le compilateur de Digital Mars ;
      - [c gdc], le compilateur de GCC ;
      - [c ldc], le compilateur adapté à l'architecture LLVM.

   [c dmd] est le compilateur qui est utilisé pendant la conception et le développement du langage D depuis des années. Tous les exemples de ce livre ont été testés avec. (NdT : l'auteur affirme que pour cette raison, ce serait plus facile pour vous de commencer avec [c dmd] et d'essayer d'autres compilateurs seulement en cas de besoin spécifique. Personnellement, j'utilise plutôt [c gdc], parce qu'il est libre, parce que je connais déjà GCC et parce qu'il est plus simple à installer chez moi, mais je n'ai pas revérifié si tous les codes du tutoriel fonctionnaient avec. À priori oui, d'autant que c'est avec ce livre que j'ai appris le D.)

   Pour installer [c dmd], rendez-vous sur la [[http://www.digitalmars.com/d/download.html | page de téléchargement de Digital Mars]] et sélectionnez la version qui correspond à votre système. n'installez pas un compilateur fait pour la version 1 du langage D. Ce livre ne couvre que la ''version 2 du D''. Le processus d'installation ne devrait pas être très compliqué. (NdT~ : les utilisateurs de Linux devraient pouvoir installer [c gdc] comme n'importe quel autre logiciel, il se trouve dans les dépôts de n'importe quelle distribution répandue.)
]
[ = Fichier source

   Le fichier que le programmeur écrit et qui est compilé par le compilateur D s'appelle le ''fichier source''. Comme D est un langage compilé, le fichier source lui-même n'est pas un programme exécutable. Il doit être converti en un programme exécutable par le compilateur.

   Comme avec n'importe quel fichier, le fichier source doit avoir un nom. Même si le nom peut être n'importe quoi, il est conseillé d'utiliser l'extension [c .d] pour le fichier source parce que les environnements de développement, les outils, les programmeurs s'attendent tous à ce que ce soit le cas. Par exemple, [c test.d], [c game.d], [c invoice.d], etc. sont des noms de fichiers sources appropriés.

]

[ = Compiler le programme hello world

   Copiez le code du programme hello world précédent dans un fichier texte et enregistrez-le sous le nom [c hello.d] (NdT~ : je vous conseille de vous créer un dossier dans lequel vous placerez tout ce dont vous aurez besoin lors de votre apprentissage du langage D).

   Le compilateur va bientôt vérifier la syntaxe de ce code source (c.-à-d. si elle est valide selon les règles du langage) et produire un programme à partir de ce code en le traduisant en code machine. Pour compiler le programme, suivez ces étapes :
      # Ouvrez un terminal.
      # Déplacez-vous vers le dossier où vous avez placé votre fichier [c hello.d].
      # Entrez la commande suivante :
         - Pour les utilisateurs de [c dmd]:
            [code=bash dmd hello.d]
         - Pour les utilisateurs de [c gdc]:
            - Si vous êtes sous windows : [code=bash gdc hello.d -o hello.exe]
            - Sinon : [code=bash gdc hello.d -o hello]
   Si vous n'avez fait aucune erreur, vous pouvez penser qu'il ne s'est rien passé. Au contraire, ça veut dire que tout s'est bien passé. Il devrait y avoir un exécutable nommé [c hello] (ou [c hello.exe] sous Windows) qui vient d'être créé par le compilateur.

   Si par contre le compilateur a affiché des messages, vous avez probablement fait une erreur. Identifiez la, corrigez la et réessayez de compiler. Vous allez régulièrement faire beaucoup d'erreurs en programmant, le processus de correction-compilation vous deviendra habituel.

   Une fois le programme créé, tapez le nom de l'exécutable pour le lancer. Vous devriez voir «~ Hello world~ » s'afficher à l'écran~ :

   [code=bash <<<
      $ ./hello      # exécution du programme.
      Hello world!   # le message qu'il affiche
   >>>]

   [p Note~ : | Le dollar ([c $]) est ici pour séparer la commande de sa sortie. Il ne faut pas l'inclure dans la commande lorsqu'on l'exécute.]

   Félicitations ! Votre programme fonctionne comme prévu.
]
[ = Paramètres du compilateur
   (NdT~ : les mentions de gdc dans cette sous-partie n'apparaissent pas dans l'œuvre originale, elle ont été ajoutée par le traducteur.)

   Le compilateur a beaucoup de paramètres en lignes de commande qui sont utilisés pour influencer la compilation du programme.
   Pour voir une liste des paramètres, les utilisateurs de dmd taperont simplement le nom de leur compilateur :
   [code=bash <<<
      $ dmd    # ← Entrez seulement le nom
      DMD64 D Compiler v2.059
      Copyright (c) 1999-2012 by Digital Mars written by Walter Bright
      Documentation: http://www.dlang.org/index.html
      Usage:
      dmd files.d ... { -switch }

      files.d      D source files
      ...
      -unittest    compile in unit tests
      ...
      -w           enable warnings
      ...
   >>>]

   NdT~ : les utilisateurs de gdc sont invités à lire la page de manuel de gdc et celle de gcc, gdc acceptant la plupart des options de gcc (pour quitter le manuel, appuyez sur "q")~ :

   [code=bash <<<
      $ man gdc
      $ man gcc
   >>>]

   L'extrait de sortie ci-avant montre seulement les paramètres que je vous recommande d'utiliser à chaque fois. Même si ça ne change rien pour le programme "hello world" de ce chapitre, la ligne de commande suivante compilera le programme en activant les avertissements et les tests unitaires. Nous verront à quoi cela correspond ainsi que d'autres paramètres du compilateur plus en détail dans les chapitres suivants~ :

   [code=bash $ dmd hello.d -w -unittest]

   NdT~ : les utilisateurs de gdc pourront utiliser cette ligne~ :
    - sous Windows :
       [code=bash $ gdc hello.d -o hello.exe -Wall -funittest]
    - autres systèmes :
       [code=bash $ gdc hello.d -o hello -Wall -funittest]

   La liste complète des paramètres de [c dmd] peut être trouvée dans la documentation du Compilateur DMD.

   Un autre paramètre de la ligne de commande que vous pouvez trouver utile est [c -run]. Il compile le code source, produit l'exécutable et le lance en une seule commande :

   [code <<<
      $ dmd -run hello.d -w -unittest
      Hello world!     ← Le programme est automatiquement exécuté
   >>>]
]
[ = IDE
   En plus du compilateur, vous pouvez aussi considérer l'installation d'un EDI (Environnement de Développement Intégré, IDE en anglais). Les EDI sont conçu pour rendre le développement de programmes plus facile en simplifiant les étapes d'écriture, de compilation et de débogage.

   Si vous installez un EDI, compiler et exécuter un programme sera aussi simple qu'appuyer sur une touche ou cliquer sur un bouton. Je vous recommande tout de même de vous familiariser avec la compilation manuelle en ligne de commande.

   Si vous décidez d'installer un EDI, allez sur  [[ http://wiki.dlang.org/IDEs | la page de dlang.org sur les EDI] pour voir la liste des EDI disponibles.
]
[ = Contenu du programme ''hello world''
   Voici une petite liste des quelques concepts du langage D qui apparaissent dans ce petit programme.
   [ = Bases du langage
      Chaque langage définit sa propre syntaxe, ses types fondamentaux, mots-clés, règles, etc. Tous ces éléments font partie des fondements du langage. Les parenthèses, les points virgules, des mots comme [c main] ou [c void] font tous partie des règles du langage D. Ils sont similaires aux règles du français : sujet, verbe, ponctuation, structures de la phrase, etc.
   ]


   [ = Bibliothèques et fonctions
      Les bases du langage ne définissent que sa structure. Elles sont utilisées pour définir des fonctions et des types de l'utilisateur, qui sont à leur tour utilisés pour créer des bibliothèques. Les bibliothèques sont des collections de parties de programmes réutilisables qui sont [c liées] à vos programmes pour les aider à atteindre leur but.

      Ci-dessus, [c writeln] est une fonction de la bibliothèque standard du langage D. Elle est utilisée, comme son nom l'indique, pour écrire une ligne ("write line").
   ]

   [ = Modules
        Les bibliothèques sont composées de parties regroupant des codes formant une certaine unité par leurs rôles, les tâches qu'ils accomplissent, etc. Ces parties forment ce qu'on appelle des modules. Le seul module que ce programme utilise est [c std.stdio], qui gère les entrées et sorties de données.
   ]

   [ = Caractères et chaînes
      Les expressions du type [c "Hello world"] sont appelées chaînes de caractères (ou simplement "chaînes"), et les éléments des chaînes sont des caractères. La seule chaîne de ce programme contient les caractères [c 'H'], [c 'e'], [c '!'], entre autres.
   ]

   [ = Ordre des opérations
       Les programmes accomplissent leurs tâches en exécutant des opérations dans un certain ordre. Ces tâches commencent avec les opérations qui sont écrites dans la fonction nommée [c main]. La seule opération dans ce programme écrit "Hello world!".
   ]

   [ = Sens des lettres minuscules et majuscules
      Vous pouvez choisir de taper n'importe quel caractère dans des chaînes, mais vous devez taper les autres caractères exactement tels qu'ils apparaissent dans le programme. C'est parce que les majuscules et les minuscules sont significatives dans les programmes D. Par exemple, [c writeln] et [c Writeln] sont deux noms différents.
   ]

  [ = Mots-clés
      Les mots spéciaux qui font partie du langage de base sont appelés [** mots-clés]. Il y a deux mots-clés dans ce programmes : [c import], qui est utilisé pour inclure un module au programme, et [c void], qui veut dire "ne retourne rien".

	  La liste complète des mots-clés de D est : [c abstract], [c alias], [c align], [c asm], [c assert], [c auto], [c body], [c bool], [c break], [c byte], [c case], [c cast], [c catch], [c cdouble], [c cent], [c cfloat], [c char], [c class], [c const], [c continue], [c creal], [c dchar], [c debug], [c default], [c delegate], [c delete], [c deprecated], [c do], [c double], [c else], [c enum], [c export], [c extern], [c false], [c final], [c finally], [c float], [c for], [c foreach], [c foreach_reverse], [c function], [c goto], [c idouble], [c if], [c ifloat], [c immutable], [c import], [c in], [c inout], [c int], [c interface], [c invariant], [c ireal], [c is], [c lazy], [c long], [c macro], [c mixin], [c module], [c new], [c nothrow], [c null], [c out], [c override], [c package], [c pragma], [c private], [c protected], [c public], [c pure], [c real], [c ref], [c return], [c scope], [c shared], [c short], [c static], [c struct], [c super], [c switch], [c synchronized], [c template], [c this], [c throw], [c true], [c try], [c typedef], [c typeid], [c typeof], [c ubyte], [c ucent], [c uint], [c ulong], [c union], [c unittest], [c ushort], [c version], [c void], [c volatile], [c wchar], [c while], [c with], [c ``__FILE__``], [c ``__MODULE__``], [c ``__LINE__``], [c ``__FUNCTION__``], [c ``__PRETTY_FUNCTION__``], [c ``__gshared``], [c ``__traits``], [c ``__vector``], and [c ``__parameters``].

	Nous les découvrirons tous dans les chapitres suivants à l'exception de [c asm] et de [c ``__vector``], qui sortent du cadre de ce livre; [c delete], [c typedef], et [c volatile] sont dépréciés; et [c macro] n'est pas utilisé pour le moment.
]

[ = Exercices

    # Modifiez le programme pour qu'il affiche autre chose.
    # Changez le programme pour qu'il affiche plus d'une ligne.
    # Essayez de compiler le programme après avoir apporté d'autres modifications ; par exemple, supprimez le point-virgule à la fin de la ligne qui contient [c writeln] et observez les erreurs de compilation.

    [[part:corrections/hello_world | … La solution]]
]
