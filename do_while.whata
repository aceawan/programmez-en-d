[set
    title       = "Boucle [c do]-[c while]"
    partAs      = chapitre
    translator  = "Raphaël Jakse"
    proofreader = "Stéphane Goujet"
]

Dans le [[part:for | chapitre sur la boucle [c for]]], nous avons vu les étapes d'exécution d'une boucle [c while]~ :

[pcode <<<
   préparation

   vérification de la condition
   ce qu'il y a à faire
   itération

   vérification de la condition
   ce qu'il y a à faire
   itération

   ...
>>>]

La boucle [c do-while] est très similaire à la boucle [c while]. La différence est que la vérification de la condition est vérifiée à la [* fin] de chaque itération de la boucle [c do-while], et donc que le corps de la boucle est exécuté au moins une fois~ :

[pcode <<<
   préparation

   ce qu'il y a à faire
   itération
   vérification de la condition    ← à la fin de l'itération

   ce qu'il y a à faire
   itération
   vérification de la condition    ← à la fin de l'itération

   ...
>>>]

Par exemple, [c do-while] peut-être plus naturel dans le programme suivant où l'utilisateur devine un nombre, puisque l'utilisateur doit faire au minimum une tentative pour deviner le nombre~ :

[code=d <<<
   import std.stdio;
   import std.random;

   void main()
   {
      int nombre = uniform(1, 101);

      writeln("Je pense à un nombre entre 1 et 100.");

      int tentative;

      do {
         write("Essayez de deviner ce nombre. ");

         readf(" %s", &tentative);

         if (nombre < tentative) {
               write("Mon nombre est plus petit que cela. ");

         } else if (nombre > tentative) {
               write("Mon nombre est plus grand que cela. ");
         }

      } while (tentative != nombre);

      writeln("Correct !");
   }
>>>]

La fonction [c uniform()] qui est utilisée dans le programme fait partie du module [c std.random]. Elle retourne un nombre dans l'intervalle spécifié. Le deuxième nombre donné en argument est hors de cet intervalle. Cela veut dire que dans ce programme, [c uniform()] ne retournera jamais 101.

[ = Exercice

   Écrivez un programme qui joue au même jeu mais qui devine le nombre que l'utilisateur donne. Si le programme est bien écrit, il devrait deviner le nombre de l'utilisateur en 7 essais au plus.

   [[part:corrections/do_while || … La solution]]
]
