[set
    title       = "Affectation et ordre d'évaluation"
    partAs      = chapitre
    translator  = "Raphaël Jakse"
    proofreader = "Stéphane Goujet"
]

Les deux premières difficultés auxquelles sont confrontés les étudiants quand ils apprennent à programmer concernent l'opération d'affectation et l'ordre d'évaluation.

[ = Opération d'affectation

   Vous verrez des lignes similaires à celle qui suit dans presque tous les programmes écrits dans presque n'importe quel langage de programmation~ :

   [code=d > a = 10;]

   Le sens de cette ligne est "changer la valeur de ''a'' par 10". De façon similaire, la ligne suivante veut dire "changer la valeur de ''b'' par 20"~ :

   [code=d > b = 20;]

   D'après cette information, que peut-on dire de cette ligne ?

   [code=d > a = b;]

   Malheureusement, la ligne n'est pas une égalité mathématique classique comme nous en avons tous l'habitude. L'expression ci-dessus ne veut pas dire "a est égal à b"~ ! En appliquant la même logique que pour les lignes précédentes, l'expression ci-dessus doit vouloir dire "changer la valeur de ''a'' par ''b''". "Changer la valeur de ''a'' par ''b''" veut dire "changer la valeur de ''a'' par la valeur de ''b''".

   Le connu symbole "=" des mathématiques a un sens complètement différent en programmation~ : changer la valeur de gauche par celle de droite.

]
[ = Ordre d'évaluation

   Les opérations d'un programme sont appliquées étape par étape dans un ordre spécifique. Nous pouvons voir les 3 expressions précédentes dans un programme dans cet ordre~ :

   [code=d <<<
      a = 10;
      b = 20;
      a = b;
   >>>]

   Le sens de ces trois lignes ensemble est~ : "changer la valeur de ''a'' par 10", ensuite changer la valeur de ''b'' par 20, ensuite changer la valeur de ''a'' par celle de ''b''". En conséquence, après que ces trois opérations sont effectuées, les valeurs de ''a'' et de ''b'' sont toutes les deux égales à 20.
]

[ = Exercice

   # [
       Observer que les trois opérations suivantes échangent les valeurs de ''a'' et de ''b''. Si leurs valeurs de départ sont respectivement 1 et 2, après les opérations les valeurs deviennent 2 et 1.

       [code=d <<<
          c = a;
          a = b;
          b = c;
        >>>]
   ]

   [[part:corrections/affectation | … La solution]]
]
