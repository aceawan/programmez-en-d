[set
    title       = "Paramètre des fonctions"
    translator  = "Raphaël Jakse"
    partAs      = correction
    proofreader = "Stéphane Gouget"
]

    Parce que les paramètres de cette fonction sont du genre de ceux qui sont copiés depuis les arguments (types valeur), ce sont ces copies qui sont échangées à l'intérieur de la fonction.
    Pour faire en sorte que la fonction échange les arguments, les deux paramètres doivent être passés par référence~ :


      [code=d <<<
         void echanger(ref int premier, ref int second)
         {
             immutable int temp = premier;
             premier = second;
             second = temp;
         }
      >>>]

      Avec ce changement, les variables dans [c main()] sont échangées~ :

      [output <<<
         2 1
      >>>]

      Même si ce n'est pas lié au problème original, notez également que [c temp] est spécifié comme [c immutable] car il n'a pas à être modifié dans la fonction après son initialisation.
