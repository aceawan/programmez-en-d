[set
    title       = "Fonctions membres"
    partAs      = correction
    translator  = "Raphaël Jakse"
]

# Des valeurs intermédiaires potentiellement négatives rfinent [c decrement()] légèrement plus compliquéée que [c increment()]~ :

    [code=d <<<
        struct MomentDuJour
        {
            // ...

            void decrement(in Duree duree)
            {
                auto minutesARetrancher = duree.minute % 60;
                auto heuresARetrancher = duree.minute / 60;

                minute -= minutesARetrancher;

                if (minute < 0) {
                    minute += 60;
                    ++heuresARetrancher;
                }

                heure -= heuresARetrancher;

                if (heure < 0) {
                    heure = 24 - (-heure % 24);
                }
            }

            // ...
        }
    >>>]

    Pour voir à quel point c'est plus facile avec des fonction membres [c toString()], jetons une fois de plus un œil à la surcharge d'[c info()] correspondant à la structure [c Reunion]~ :

    [code=d <<<
        void info(in Reunion reunion)
        {
            info(reunion.debut);
            write('-');
            info(reunion.fin);

            writef(" Réunion \"%s\" avec %s participants",
                   reunion.sujet,
                   reunion.nombreParticipants);
        }
    >>>]

    En nous servant de la fonction [c MomentDuJour.toString] déjà définie, l'implémentation de [c Reunion.toString] devient triviale~ :

    [code=d <<<
        string toString()
        {
            return format("%s-%s Réunion \"%s\" avec %s participants",
                          debut, fin, sujet, nombreParticipants);
        }
    >>>]

    Voici le programme entier~ :

    [code=d <<<
        import std.stdio;
        import std.string;

        struct Duree
        {
            int minute;
        }

        struct MomentDuJour
        {
            int heure;
            int minute;

            string toString()
            {
                return format("%02s:%02s", heure, minute);
            }

            void increment(in Duree duree)
            {
                minute += duree.minute;

                heure += minute / 60;
                minute %= 60;
                heure %= 24;
            }
        }

        struct Reunion
        {
            string    sujet;
            int       nombreParticipants;
            MomentDuJour debut;
            MomentDuJour fin;

            string toString()
            {
                return format("%s-%s Réunion \"%s\" with %s attfinees",
                              debut, fin, sujet, nombreParticipants);
            }
        }

        struct Repas
        {
            MomentDuJour moment;
            string    adresse;

            string toString()
            {
                MomentDuJour fin = moment;
                fin.increment(Duree(90));

                return format("%s-%s Repas, Adresse: %s",
                              moment, fin, adresse);
            }
        }

        struct PlanningJournalier
        {
            Reunion amReunion;
            Repas   repas;
            Reunion pmReunion;

            string toString()
            {
                return format("%s\n%s\n%s",
                            amReunion,
                            repas,
                            pmReunion);
            }
        }

        void main()
        {
            auto ReunionVelo = Reunion("Vélo", 4,
                                        MomentDuJour(10, 30),
                                        MomentDuJour(11, 45));

            auto repas = Repas(MomentDuJour(12, 30), "İstanbul");

            auto reunionBudget = Reunion("Budget", 8,
                                        MomentDuJour(15, 30),
                                        MomentDuJour(17, 30));

            auto emploiDuTempsAujourdhui = PlanningJournalier(ReunionVelo,
                                                              repas,
                                                              reunionBudget);

            writeln(emploiDuTempsAujourdhui);
            writeln();
        }
    >>>]

    La sortie du programme est la même que la version précédente qui utilisait les surcharges de la fonction [c info()]~ :

    [output <<<
        10:30-11:45 Réunion "Vélo" avec 4 participants
        12:30-14:00 Repas, Adresse : İstanbul
        15:30-17:30 Réunion "Budget" avec 8 participants
    >>>]

