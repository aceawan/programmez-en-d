[set
    title       = "Les énumérations ([c enum])"
    translator  = "Raphaël Jakse"
    partAs      = correction
    proofreader = "Stéphane Goujet"
]

[code=d <<<
    import std.stdio;
    import std.conv;

    enum Operation { sortir, ajouter, soustraire, multiplier, diviser }

    void main()
    {
        // Afficher les opérations prises en charge
        write("Opérations - ");
        for (Operation operation;
            operation <= Operation.max;
            ++operation) {

            writef("%d:%s ", operation, operation);
        }
        writeln();

        // Boucle infinie jusqu'à ce que l'utilisateur veuille sortir
        while (true) {
            write("Opération ? ");

            // L'entrée doit être lue dans le type réel (int) de l'énumération
            int codeOperation;
            readf(" %s", &codeOperation);

            /*
            À partir d'ici, on utilise les valeurs de l'énumération
            plutôt que les constantes magiques. Le code de l'opération
            qui a été lu doit donc être converti vers sa valeur énumérée
            correspondante.

            (Les conversions de types seront traitées plus en détail dans un
            chapitre ultérieur.)
            */
            Operation operation = cast(Operation)codeOperation;

            if ((operation < Operation.min) ||
                (operation > Operation.max)) {
                writeln("ERREUR : Opération invalide");
                continue;
            }

            if (operation == Operation.sortir) {
                writeln("Au revoir !");
                break;
            }

            double premier;
            double second;
            double resultat;

            write("Premier opérande ? ");
            readf(" %s", &premier);

            write("Second opérande  ? ");
            readf(" %s", &second);

            switch (operation) {

            case Operation.ajouter:
                resultat = premier + second;
                break;

            case Operation.soustraire:
                resultat = premier - second;
                break;

            case Operation.multiplier:
                resultat = premier * second;
                break;

            case Operation.diviser:
                resultat = premier / second;
                break;

            default:
                throw new Exception(
                    "ERREUR: Cette ligne n'aurait jamais dû être atteinte.");
            }

            writeln("        resultat : ", resultat);
        }
    }
>>>]
