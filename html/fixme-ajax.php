<?php

/*
 * Copyright (c) Raphaël JAKSE, 2014
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */

    header('Content-type: application/json; charset=utf-8');
    if (isset($_GET['u'])) {
        $dirname = realpath(dirname($_SERVER['DOCUMENT_ROOT'] . $_SERVER['PHP_SELF']));
        $realpath = realpath($_SERVER['DOCUMENT_ROOT'] . $_GET['u']);
        if ($realpath && (strpos($realpath, $dirname) === 0)) {
            $f = substr($realpath, strlen($dirname));

            if (!is_dir('fixme_data')) {
                @mkdir('fixme_data');
            }

            $f = 'fixme_data/' . base64_encode($f);
        } else {
            $f = '';
        }

        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            if (strlen($HTTP_RAW_POST_DATA) > 1024 * 500) {
                echo '{"error":2,"errorText":"La requête est trop grosse."}';
                return;
            }

            if ($f) {
                if (is_file($f) && filesize($f) > 20 * 1024 * 500) {
                    echo '{"error":3,"errorText":"Trop de corrections déjà proposées."}';
                }
                $d = @json_decode($HTTP_RAW_POST_DATA);
                if ($d && isset($d->passage)) {
                    if (@file_put_contents($f, json_encode($d) . "\n", FILE_APPEND)) {
                        echo '{"error":0}';
                    } else {
                        echo '{"error":4,"errorText":"impossible de sauvegarder la correction."}';
                    }
                } else {
                    echo '{"error":1,"errorText":"requête mal formée."}';
                }
            } else {
                echo '{"error":5,"errorText":"le fichier donné n\'est pas géré par Fixme."}';
            }
        } else if ($f && ($f = @fopen($f, 'r'))) {
            echo '['; $comma = false;
            while (($line = fgets($f))) {
                if ($comma) {
                    echo ',';
                } else {
                    $comma = ',';
                }
                $d = json_decode($line);
                echo '{"passage":' . json_encode($d->passage) . '}';
            }
            echo ']';
            fclose($f);
        } else {
            echo '[]';
        }
    } else {
        echo '{"error":1,"errorText":"Merci d\'indiquer la page à laquelle vous vous intéressez avec le paramètre u (post pour envoyer une correction, get pour les récupérer)."}';
    }
?>